//TEMPLATE fuer die Datensammlung

//Ersetze <NAME> durch Vornamen
function data<NAME>(){
	//------Montag-----------------------------------------------------------------
	var actOfMon = [];
	
	//Ersetze <NAME> durch Namen der Taetigkeit, <ZEIT> durch Dauer in Stunden, zB .push(new action("Volleyball", 2));
	//Diese Zeile kann beliebig oft kopiert werden

	actOfMon.push(new action("<NAME>", <ZEIT>));
	actOfMon.push(new action("<NAME>", <ZEIT>));
	actOfMon.push(new action("<NAME>", <ZEIT>));
	
	//------Dienstag---------------------------------------------------------------
	var actOfTue = [];
	
	//Analog zum Montag ausfuellen, mit den restlichen Tagen genauso verfahren
	//ACHTUNG: beim Kopieren der Zeile drauf achten dass ihr vom richtigen Tag kopiert
	
	actOfTue.push(new action("<NAME>", <ZEIT>));
	
	//------Mittwoch---------------------------------------------------------------
	var actOfWed = [];
	
	actOfWed.push(new action("<NAME>", <ZEIT>));
	
	//------Donnerstag-------------------------------------------------------------
	var actOfThu = [];
	
	actOfThu.push(new action("<NAME>", <ZEIT>));
	
	//------Freitag----------------------------------------------------------------
	var actOfFri = [];
	
	actOfFri.push(new action("<NAME>", <ZEIT>));
	
	//------Samstag----------------------------------------------------------------
	var actOfSat = [];
	
	actOfSat.push(new action("<NAME>", <ZEIT>));
	
	//------Sonntag----------------------------------------------------------------
	var actOfSun = [];
	
	actOfSun.push(new action("<NAME>", <ZEIT>));
		
	var allDays = [];
	allDays.push(new day("Monday", actOfMon));
	allDays.push(new day("Tuesday", actOfTue));
	allDays.push(new day("Wednesday", actOfWed));
	allDays.push(new day("Thursday", actOfThu));
	allDays.push(new day("Friday", actOfFri));
	allDays.push(new day("Saturday", actOfSat));
	allDays.push(new day("Sunday", actOfSun));
	
	//Ersetze <NAME> durch Vornamen
	var per = new person("<NAME>", allDays);
	
	return per;
}