function dataAlex(){
	//------Montag-----------------------------------------------------------------
	var actOfMon = [];

	actOfMon.push(new action("Schlafen", 9));
	actOfMon.push(new action("Duschen", 0.5));
	actOfMon.push(new action("Fernsehen", 1));
	actOfMon.push(new action("Packen", 0.25));
	actOfMon.push(new action("Auto fahren", 0.25));
	actOfMon.push(new action("Warten", 0.25));
	actOfMon.push(new action("Physiotherapie", 0.5));
	actOfMon.push(new action("Auto fahren", 0.25));
	actOfMon.push(new action("Vorbereitung", 1.25));
	actOfMon.push(new action("Essen", 0.5));
	actOfMon.push(new action("Warten", 0.5));
	actOfMon.push(new action("Termin", 0.75));
	actOfMon.push(new action("Vorbereitung", 1));
	actOfMon.push(new action("Quatschen", 0.75));
	actOfMon.push(new action("Lehrveranstaltung besuchen", 1.5));
	actOfMon.push(new action("Gruppenarbeit Treffen", 1));
	actOfMon.push(new action("Auto fahren", 0.25));
	actOfMon.push(new action("Duschen", 0.25));
	actOfMon.push(new action("Kochen", 0.25));
	actOfMon.push(new action("Online surfen", 0.25));
	actOfMon.push(new action("Fernsehen", 0.75));
	actOfMon.push(new action("Chatten", 3));
	//------Dienstag---------------------------------------------------------------
	var actOfTue = [];
	
    actOfTue.push(new action("Schlafen", 7.5));
	actOfTue.push(new action("Essen", 0.25));
	actOfTue.push(new action("Zähne putzen und Einpacken", 0.25));
    actOfTue.push(new action("Fernsehen", 0.25));
    actOfTue.push(new action("Einkaufen", 0.25));
    actOfTue.push(new action("Auto fahren", 0.25));
    actOfTue.push(new action("Lehrveranstaltung besuchen", 1.5));
    actOfTue.push(new action("Vorbereitung", 0.5));
    actOfTue.push(new action("Online surfen", 1.25));
    actOfTue.push(new action("Lehrveranstaltung besuchen", 1.5));
	actOfTue.push(new action("Online surfen", 0.75));
    actOfTue.push(new action("Gruppenarbeit Treffen", 1.5));
    actOfTue.push(new action("Auto fahren", 0.25));
    actOfTue.push(new action("Essen", 0.5));
    actOfTue.push(new action("Auto fahren", 0.25));
    actOfTue.push(new action("Zu Fuß gehen", 0.25));
	actOfTue.push(new action("Quatschen", 0.5));
    actOfTue.push(new action("Kino", 2.25));
    actOfTue.push(new action("Quatschen", 0.5));
    actOfTue.push(new action("Auto fahren", 0.25));
    actOfTue.push(new action("Schlafen", 0.75));
	
	//------Mittwoch---------------------------------------------------------------
	var actOfWed = [];
	
	actOfWed.push(new action("Schlafen", 7));
	actOfWed.push(new action("Essen", 0.25));
	actOfWed.push(new action("Zähne putzen und Einpacken", 0.25));
    actOfWed.push(new action("Fernsehen", 0.5));
    actOfWed.push(new action("Auto fahren", 0.25));
	actOfWed.push(new action("Physiotherapie", 0.5));
	actOfWed.push(new action("Auto fahren", 0.25));
    actOfWed.push(new action("Einkaufen", 0.25));
    actOfWed.push(new action("Auto fahren", 0.25));
    actOfWed.push(new action("Arbeiten", 3.25));
    actOfWed.push(new action("Essen", 0.5));
    actOfWed.push(new action("Arbeiten", 0.5));
    actOfWed.push(new action("Quatschen", 1.5));
    actOfWed.push(new action("Arbeiten", 2.5));
    actOfWed.push(new action("Reiten", 1));
    actOfWed.push(new action("Auto fahren", 0.25));
    actOfWed.push(new action("Online surfen", 2.75));
    actOfWed.push(new action("Chatten", 2));
	
	//------Donnerstag-------------------------------------------------------------
	var actOfThu = [];
	
	actOfThu.push(new action("Schlafen", 6));
	actOfThu.push(new action("Duschen", 0.25));
	actOfThu.push(new action("Essen", 0.25));
	actOfThu.push(new action("Zähne putzen und Einpacken", 0.25));
	actOfThu.push(new action("Öffis nutzen", 0.25));
	actOfThu.push(new action("Einkaufen", 0.25));
	actOfThu.push(new action("Zu Fuß gehen", 0.25));
	actOfThu.push(new action("Frisör", 1.5));
	actOfThu.push(new action("Öffis nutzen", 0.25));
	actOfThu.push(new action("Vorbereitung", 1));
	actOfThu.push(new action("Quatschen", 1));
	actOfThu.push(new action("Öffis nutzen", 0.5));
	actOfThu.push(new action("Faulenzen", 0.5));
	actOfThu.push(new action("Vorbereitung", 1));
	actOfThu.push(new action("Essen", 0.5));
	actOfThu.push(new action("Faulenzen", 0.5));
	actOfThu.push(new action("Einkaufen", 2));
	actOfThu.push(new action("Faulenzen", 0.75));
	actOfThu.push(new action("Auto fahren", 0.25));
	actOfThu.push(new action("Essen", 0.5));
	actOfThu.push(new action("Quatschen", 0.5));
	actOfThu.push(new action("Auto fahren", 0.25));
	actOfThu.push(new action("Chatten", 0.5));
	actOfThu.push(new action("Schlafen", 2.25));
	
	//------Freitag----------------------------------------------------------------
	var actOfFri = [];
	
	actOfFri.push(new action("Schlafen", 8.5));
	actOfFri.push(new action("Online surfen", 0.75));
	actOfFri.push(new action("Essen", 0.25));
	actOfFri.push(new action("Duschen", 0.25));
	actOfFri.push(new action("Zähne putzen und Einpacken", 0.25));
	actOfFri.push(new action("Vorbereitung", 2));
	actOfFri.push(new action("Öffis nutzen", 0.25));
	actOfFri.push(new action("Warten", 0.75));
	actOfFri.push(new action("Lehrveranstaltung besuchen", 0.75));
	actOfFri.push(new action("Auto fahren", 0.25));
	actOfFri.push(new action("Einkaufen", 2.75));
	actOfFri.push(new action("Auto fahren", 0.25));
	actOfFri.push(new action("Schlafen", 1.75));
	actOfFri.push(new action("Essen", 0.25));
	actOfFri.push(new action("Schlafen", 4.25));
	
	//------Samstag----------------------------------------------------------------
	var actOfSat = [];
	
	actOfSat.push(new action("Schlafen", 8));
	actOfSat.push(new action("Duschen", 0.25));
	actOfSat.push(new action("Essen", 0.25));
	actOfSat.push(new action("Zähne putzen und Einpacken", 0.25));
	actOfSat.push(new action("Faulenzen", 0.25));
	actOfSat.push(new action("Auto fahren", 0.25));
	actOfSat.push(new action("Reiten", 1.75));
	actOfSat.push(new action("Quatschen", 0.5));
	actOfSat.push(new action("Auto fahren", 0.5));
	actOfSat.push(new action("Faulenzen", 1));
	actOfSat.push(new action("Quatschen", 1));
	actOfSat.push(new action("Arbeiten", 3));
	actOfSat.push(new action("Reiten", 1.5));
	actOfSat.push(new action("Quatschen", 0.25));
	actOfSat.push(new action("Auto fahren", 0.25));
	actOfSat.push(new action("Fernsehen", 0.5));
	actOfSat.push(new action("Essen", 0.5));
	actOfSat.push(new action("Duschen", 0.25));
	actOfSat.push(new action("Videospiele spielen", 0.75));
	actOfSat.push(new action("Chatten", 2));
	actOfSat.push(new action("Fernsehen", 1));
	
	//------Sonntag----------------------------------------------------------------
	var actOfSun = [];
	
	actOfSun.push(new action("Fernsehen", 1));
	actOfSun.push(new action("Schlafen", 1.5));
	actOfSun.push(new action("Chatten", 0.5));
	actOfSun.push(new action("Aufräumen", 1));
	actOfSun.push(new action("Film schauen", 6));
	actOfSun.push(new action("Essen", 0.5));
	actOfSun.push(new action("Film schauen", 1.5));
	actOfSun.push(new action("Schlafen", 0.5));
	actOfSun.push(new action("Aufräumen", 0.5));
	actOfSun.push(new action("Videospiele spielen", 1.5));
	actOfSun.push(new action("Film schauen", 4.5));
	actOfSun.push(new action("Kochen", 0.5));
	actOfSun.push(new action("Essen", 0.5));
	actOfSun.push(new action("Quatschen", 1.5));
	actOfSun.push(new action("Faulenzen", 0.5));
	actOfSun.push(new action("Chatten", 0.5));
	actOfSun.push(new action("Schlafen", 1.5));
		
	var allDays = [];
	allDays.push(new day("Montag", actOfMon));
	allDays.push(new day("Dienstag", actOfTue));
	allDays.push(new day("Mittwoch", actOfWed));
	allDays.push(new day("Donnerstag", actOfThu));
	allDays.push(new day("Freitag", actOfFri));
	allDays.push(new day("Samstag", actOfSat));
	allDays.push(new day("Sonntag", actOfSun));
	
	var per = new person("Krien", allDays);
	
	return per;
}