function people(persons){
	this.name = "Alle Personen";
	this.nodes = persons;
}

people.prototype.calculateWeight = function(){
	var weight = 0;
	for (var i = this.nodes.length; i--; i >= 0){
		weight += this.nodes[i].calculateWeight();
	}
	return weight;
}

people.prototype.getRequestedPerson = function(evt){
	var name = getPerson(evt.target.getAttribute('person'));
	
	for(var j = this.nodes.length; j--; j >= 0){
		if(this.nodes[j].name == name){
			return this.nodes[j];
		}
	}
	return null;
}

people.prototype.addPerson = function(evt){
	var person = data.getRequestedPerson(evt);
	
	this.nodes.push(person);
}

people.prototype.deletePerson = function(evt){
	var name = getPerson(evt.target.getAttribute('person'));
	
	for(var i = this.nodes.length; i--; i >= 0){
		if(this.nodes[i].name == name){
			this.nodes.splice(i, 1);
		}
	}
}

people.prototype.sortActionsByCategoryForAllPersons = function(){
	for(var i = this.nodes.length; i--; i >= 0){
		for(var j = this.nodes[i].nodes.length; j--; j >= 0){
			for(var k = this.nodes[i].nodes[j].actions.length; k--; k>= 0){
				this.nodes[i].nodes[j].nodes[this.nodes[i].nodes[j].actions[k].category].actions.push(this.nodes[i].nodes[j].actions[k]);
			}
			
		}
	}
	
	for(var i = this.nodes.length; i--; i >= 0){
		for(var j = this.nodes[i].nodes.length; j--; j >= 0){
			for(var k = this.nodes[i].nodes[j].nodes.length; k--; k>= 0){
				if(this.nodes[i].nodes[j].nodes[k].actions == ""){
					this.nodes[i].nodes[j].nodes.splice(k, 1);
				}
			}
		}
	}
}

function getPerson(per){
	switch (per){
		case '0': return "Krien";
		case '1': return "Chong";
		case '2': return "Gromykina";
		case '3': return "Rehak";
		case '4': return "Davydkina";
	}
}

function person(name, days){
	this.name = name;
	this.nodes = days;
	this.active = 1;
}

person.prototype.calculateWeight = function(){
	var weight = 0;
	for (var i = this.nodes.length; i--; i >= 0){
		weight += this.nodes[i].calculateWeight();
	}
	return weight;
}

function day(name, actions){
	this.name = name;
	this.actions = actions;
	this.nodes = [new category("Uni & Arbeit"),new category("Grundbeduerfnisse"),new category("Unterwegs"),new category("Freizeit"),new category("Soziale Aktivitäten"),new category("Digitale Medien"),new category("Sport")];
	this.active = 1;
}

function getDay(d){
	switch(d){
		case '0': return "Montag";
		case '1': return "Dienstag";
		case '2': return "Mittwoch";
		case '3': return "Donnerstag";
		case '4': return "Freitag";
		case '5': return "Samstag";
		case '6': return "Sonntag";

	}
}

day.prototype.calculateWeight = function(){
	var weight = 0;
	for (var i = this.nodes.length; i--; i >= 0){
		weight += this.nodes[i].calculateWeight();
	}
	return weight;
}

function category(name){
	this.name = name;
	this.actions = [];
	this.nodes = "end";
	this.time = this.calculateWeight();
	this.category = searchForCategory(this.name, categories);
	this.active = 1;
}

category.prototype.calculateWeight = function(){
	var weight = 0;
	for (var i = this.actions.length; i--; i >= 0){
		weight += this.actions[i].calculateWeight();
	}
	this.time = weight;
	return weight;
}

function action(name, time){
	this.name = name;
	this.time = time;
	this.nodes = "endAct";
	this.actions = 0;
	this.category = searchForCategory(this.name, categories);
	this.subcategory = searchForCategory(this.name, subcategories);
	this.subsubcategory = searchForCategory(this.name, subsubcategories);
}

action.prototype.calculateWeight = function(){
	return this.time;
}



/*-------------ALGORITHM-------------------------------------*/
/*-------------INITIALIZE------------------------------------*/

function start(){
    //do something
}

/*-------------DATA BASE-------------------------------------*/
/*-------------CATEGORY-------------------------------------*/
var uniarb = [(0), ("Uni & Arbeit"), ("Vorbereitung"), ("Lehrveranstaltung besuchen"), ("Gruppenarbeit Treffen"), ("Übungen aufbereiten"), ("Proseminararbeit schreiben"), ("Arbeiten")];
var lernen = [(0.1), ("Lernen"), ("Vorbereitung"), ("Übungen aufbereiten"), ("Proseminararbeit schreiben")];
var uni = [(0.2), ("Uni"), ("Lehrveranstaltung besuchen"), ("Gruppenarbeit Treffen")];
var arb = [(0.3), ("Arbeiten"), ("Arbeiten")];

var grund = [(1), ("Grundbeduerfnisse"), 
				("Toilette nutzen"), ("Duschen"), ("Zähne putzen und Einpacken"), ("Umziehen"), ("Frisör"),
				("Kochen"), ("Essen"),
				("Abwaschen"), ("Waschen"), ("Aufräumen"), ("Packen"), ("Auspacken"),
				("Einkaufen"), ("Besorgungen machen"), ("Briefe schreiben"), ("Post lesen"), ("Termin"),
				("Schlafen"),
				("Physiotherapie")];
var koerper =[(11), ("Körperpflege"), ("Toilette nutzen"), ("Duschen"), ("Zähne putzen und Einpacken"), ("Umziehen"), ("Frisör")];
var hyg =[(111), ("Hygiene"), ("Toilette nutzen"), ("Duschen"), ("Zähne putzen und Einpacken")];
var schoen = [(112), ("Schönheit"), ("Umziehen"), ("Frisör")];
var essen = [(12), ("Nahrung"), ("Kochen"), ("Essen")];
var ordnung = [(13), ("Ordnung"), ("Abwaschen"), ("Waschen"), ("Aufräumen"), ("Packen"), ("Auspacken")];
var verpflichtungen = [(14), ("Verpflichtungen & Besorgungen"), ("Einkaufen"), ("Besorgungen machen"), ("Briefe schreiben"), ("Post lesen"), ("Termin")];
var gesundheit = [(15), ("Gesundheit"), ("Physiotherapie")];
var schlafen = [(16), ("Schlafen"), ("Schlafen")];

var unterwegs = [(2), ("Unterwegs"), ("Fahrrad fahren"), ("Auto fahren"), ("Öffis nutzen"), ("Flug"), ("Zu Fuß gehen"), ("Warten"), ("Zug fahren")];
var allein = [(21), ("Allein"), ("Fahrrad fahren"), ("Auto fahren"), ("Zu Fuß gehen")];
var oeffentlich = [(22), ("Öffentliche Verkehrsmittel"), ("Öffis nutzen"), ("Flug"), ("Zug fahren")];
var warten = [(23), ("Warten"), ("Warten")];

var freizeit = [(3), ("Freizeit"), 
				("Geschichte schreiben"), ("Fotografieren"), ("Basteln"), ("Karten lesen"), ("Buch lesen"), ("Sprachen lernen"),
				("Ausruhen"), ("Faulenzen"),
				("Spazieren gehen"), ("Wandern"), ("Garten machen"),
				("Mit Kindern Zeit verbringen"), ("Kino")];
var bildung = [(31), ("Bildung"), ("Geschichte schreiben"), ("Fotografieren"), ("Basteln"), ("Karten lesen"), ("Buch lesen"), ("Sprachen lernen")];
var ruhe = [(32), ("Ruhe"), ("Ausruhen"), ("Faulenzen")];
var natur = [(33), ("Natur"), ("Spazieren gehen"), ("Wandern"), ("Garten machen")];
var sonst = [(34), ("Sonstiges"), ("Mit Kindern Zeit verbringen"), ("Kino")];
				
var sozial = [(4), ("Soziale Aktivitäten"), ("Quatschen"), ("Mit Freunden treffen"), ("Ausgehen")];

var digital = [(5), ("Digitale Medien"), ("Handy nutzen"), ("Chatten"), ("Anrufen/Skypen"), ("Computer spielen"), ("Videospiele spielen"), ("Online surfen"), ("Film schauen"), ("Fernsehen")];
var pc = [(51), ("Computer & Konsolen"), ("Chatten"), ("Computer spielen"), ("Videospiele spielen"), ("Online surfen")];
var handy = [(52), ("Handy"), ("Handy nutzen"), ("Anrufen/Skypen")];
var tv = [(53), ("Fernseher"), ("Film schauen"), ("Fernsehen")];

var sport = [(6), ("Sport"), ("Salsa"), ("Rollschuhe fahren"), ("Klettern"), ("Schwimmen"), ("Slackline"), ("Yoga"), ("Reiten"), ("Fitnesstudio"), ("Volleyball")];

var categories = [uniarb, grund, unterwegs, freizeit, sozial, digital, sport];
var subcategories = 	[lernen, uni, arb,
						koerper, ordnung, verpflichtungen, gesundheit, schlafen, 
						allein, oeffentlich, warten,
						essen, bildung, ruhe, natur, sonst, 
						sozial,
						pc, handy, tv,
						sport];
var subsubcategories = [hyg, schoen];

function searchForCategory(name, dataCat){
	for(var h = dataCat.length; h--; h >= 0){
		for(var i = dataCat[h].length; i--; i >= 1){
			if(dataCat[h][i] == name) return dataCat[h][0];
		}
	}

	return undefined;
}